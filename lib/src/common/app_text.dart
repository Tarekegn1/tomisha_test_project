import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppText {
  static Text boldText(String text, Color color) {
    return Text(
      text,
      style: GoogleFonts.lato(fontWeight: FontWeight.bold, fontSize: 65, color: color),
      textAlign: TextAlign.left,
    );
  }

  static Text mediumText(String text, double size, Color color) {
    return Text(
      text,
      style: GoogleFonts.lato(fontWeight: FontWeight.w500, fontSize: size, color: color),
      textAlign: TextAlign.center,
    );
  }

  static Text regularText(String text, double size, Color color) {
    return Text(
      text,
      style: GoogleFonts.lato(fontWeight: FontWeight.normal, fontSize: size, color: color),
    );
  }

  static Text semiBoldText(String text, double size, Color color) {
    return Text(
      text,
      style: GoogleFonts.lato(fontWeight: FontWeight.w500, fontSize: size, color: color),
    );
  }
}
