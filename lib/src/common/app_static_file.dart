class AppStaticFile {
  static const String agreementImage = "assets/images/agreement.png";
  static const String profileDataImage = "assets/images/profileData.png";
  static const String businessCard = "assets/images/businesscard.png";
  static const String jobOffset = "assets/images/jobOffer.png";
  static const String undrawImage = "assets/images/undraw.png";
  static const String personalImage = "assets/images/personal.png";
  static const String stepOneImage = "assets/images/line1.png";
  static const String stepTwoImage = "assets/images/line2.png";
}
