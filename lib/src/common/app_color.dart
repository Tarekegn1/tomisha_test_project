import 'package:flutter/material.dart';

class AppColor {
  static const Color darkGreen = Color.fromRGBO(49, 151, 149, 1);
  static const Color darkBlue = Color.fromRGBO(49, 130, 206, 1);
  static const Color lightGreen = Color.fromRGBO(129, 230, 217, 1);
  static const Color darkBlack = Color.fromRGBO(45, 55, 72, 1);
  static const Color darkGray = Color.fromRGBO(74, 85, 104, 1);
  static const Color opaqueGreen = Color.fromRGBO(230, 255, 250, 1);
  static const Color opaqueGreenshGray = Color.fromRGBO(235, 244, 255, 1);
  static const Color darkGrayish = Color.fromRGBO(113, 128, 150, 1);
  static const Color creamGraysh = Color.fromRGBO(0, 0, 0, 0.16);
  static const Color lightGray = Color.fromRGBO(203, 213, 224, 1);
  static const Color opaqueLightGray = Color.fromRGBO(247, 250, 252, 1);
  static const Color whiteColor = Color.fromRGBO(255, 255, 255, 1);
  static const Color topShadowColor = Color.fromRGBO(0, 0, 0, 0.16);
}
