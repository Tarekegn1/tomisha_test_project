import 'package:flutter/material.dart';

class CustomSecondWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height - 10);

    var firstPoint = Offset(size.width / 4, size.height - 50);
    var firstEnd = Offset(size.width / 2 + 10, size.height - 5);
    path.quadraticBezierTo(firstPoint.dx, firstPoint.dy, firstEnd.dx, firstEnd.dy);

    var secondP = Offset(size.width - (size.width / 4), size.height + 10);
    var secondEnd = Offset(size.width, size.height - 80);

    path.quadraticBezierTo(secondP.dx, secondP.dy, secondEnd.dx, secondEnd.dy);

    path.lineTo(size.width, 30);
    var thirdP = Offset(size.width - (size.width / 6), 0);
    var thirdEnd = Offset(
      size.width - (size.width / 3.5),
      30,
    );
    path.quadraticBezierTo(thirdP.dx, thirdP.dy, thirdEnd.dx, thirdEnd.dy);

    var fourP = Offset(size.width / 2, 80);
    var foundEnd = Offset(size.width / 4, 20);
    path.quadraticBezierTo(fourP.dx, fourP.dy, foundEnd.dx, foundEnd.dy);

    //
    var fifthP = Offset(size.width / 8, 0);
    var fifthEnd = const Offset(
      0,
      10,
    );
    path.quadraticBezierTo(fifthP.dx, fifthP.dy, fifthEnd.dx, fifthEnd.dy);

    path.close();

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return false;
  }
}
