import 'package:flutter/material.dart';
import 'package:test_project/src/components/large_screen_hero_component.dart';
import 'package:test_project/src/components/large_step_component.dart';
import 'package:test_project/src/components/medium_step_component.dart';
import 'package:test_project/src/components/middle_screen_hero_component.dart';
import 'package:test_project/src/components/small_screen_footer.dart';
import 'package:test_project/src/components/small_steps_component.dart';
import 'package:test_project/src/components/tab_menu_component.dart';
import 'package:test_project/src/components/nav_bar_component.dart';
import 'package:test_project/src/components/small_screen_hero_component.dart';
import 'package:test_project/src/util/response_widget.dart';

class LandingScreen extends StatefulWidget {
  const LandingScreen({super.key});

  @override
  State<LandingScreen> createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {
  ScrollController scrollController = ScrollController();
  Offset _initialOffset = const Offset(0, 400);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Stack(
          children: [
            _buildHeaderNavBar(),
            Positioned(
              top: 70,
              left: 0,
              right: 0,
              bottom: 0,
              child: SingleChildScrollView(
                controller: scrollController,
                child: Column(
                  children: [
                    _buildHeroWidget(context),
                    const TabMenuComponent(),
                    _buildBodyContent(context),
                  ],
                ),
              ),
            ),
            if (ResponsiveWidget.isSmallScreen(context)) _buildFooter()
          ],
        ),
      ),
    );
  }

  void scrollDownWithAnimating() {
    scrollController.animateTo(_initialOffset.dy, duration: const Duration(milliseconds: 500), curve: Curves.ease);
    setState(() {
      _initialOffset = Offset(0, _initialOffset.dy);
    });
  }

  Widget _buildHeroWidget(BuildContext context) {
    return ResponsiveWidget(
      largeScreen: LargeScreenHeroComponent(
        onClicked: scrollDownWithAnimating,
      ),
      mediumScreen: MiddleScreenHeroComponent(
        onClicked: scrollDownWithAnimating,
      ),
      smallScreen: const SmallScreenHeroComponent(),
    ).build(context);
  }

  Widget _buildBodyContent(BuildContext context) {
    return ResponsiveWidget(
            largeScreen: const LargeStepComponent(),
            mediumScreen: const MediumStepsComponent(),
            smallScreen: const SmallStepsComponent())
        .build(context);
    // return ResponsiveWidget.isSmallScreen(context) ? const SmallStepsComponent() : const LargeStepComponent();
  }

  Widget _buildHeaderNavBar() {
    return const Positioned(
      top: 0,
      left: 0,
      right: 0,
      child: NavBarComponent(),
    );
  }

  Widget _buildFooter() {
    return const Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: SmallScreenFooter(),
    );
  }
}
