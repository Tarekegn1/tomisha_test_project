import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CustomButton extends StatefulWidget {
  final VoidCallback onClicked;
  const CustomButton({super.key, required this.onClicked});

  @override
  State<CustomButton> createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  bool onHovered = false;
  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onHover: (val) {
        setState(() {
          onHovered = true;
        });
      },
      onExit: (v) {
        setState(() {
          onHovered = false;
        });
      },
      child: GestureDetector(
        onTap: widget.onClicked,
        child: Container(
          height: 40,
          width: 320,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(12),
              gradient: LinearGradient(
                colors: [
                  onHovered ? const Color.fromRGBO(44, 122, 123, 1) : const Color.fromRGBO(49, 151, 149, 1),
                  onHovered ? const Color.fromRGBO(43, 108, 176, 1) : const Color.fromRGBO(49, 130, 206, 1),
                ],
              )),
          child: Center(
            child: Text(
              "Kostenlos Registrieren",
              style: GoogleFonts.lato(
                color: const Color.fromRGBO(230, 255, 250, 1),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
