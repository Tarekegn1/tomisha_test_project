import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:test_project/src/common/app_color.dart';
import 'package:test_project/src/data/mock_data.dart';
import 'package:test_project/src/util/response_widget.dart';

class TabMenuComponent extends StatefulWidget {
  const TabMenuComponent({super.key});

  @override
  State<TabMenuComponent> createState() => _TabMenuComponentState();
}

class _TabMenuComponentState extends State<TabMenuComponent> {
  int currentIndex = 0;
  int onHoverIndex = -1;
  final _menuScrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          const SizedBox(height: 20),
          ClipRRect(
            borderRadius: BorderRadius.circular(12),
            child: Container(
              alignment: Alignment.center,
              width: ResponsiveWidget.isSmallScreen(context) ? 400 : 480,
              height: 40,
              decoration: BoxDecoration(
                color: AppColor.lightGray,
                borderRadius: BorderRadius.circular(12),
                border: Border.all(
                  color: AppColor.lightGray,
                ),
              ),
              child: ListView.separated(
                controller: _menuScrollController,
                scrollDirection: Axis.horizontal,
                itemCount: 3,
                itemBuilder: (context, index) {
                  return SizedBox(
                    width: 160,
                    child: MouseRegion(
                      onHover: (val) {
                        setState(() {
                          onHoverIndex = index;
                        });
                      },
                      onExit: (val) {
                        setState(() {
                          onHoverIndex = -1;
                        });
                      },
                      child: GestureDetector(
                        onTap: () {
                          setState(() {
                            currentIndex = index;
                            if (ResponsiveWidget.isSmallScreen(context)) {
                              scrollToCenter(index, screenWidth);
                            }
                          });
                        },
                        child: _buildMenuItem(
                          currentIndex == index,
                          MockData.menuData[index]["name"],
                          onHoverIndex == index,
                        ),
                      ),
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return const SizedBox(
                    width: 1.5,
                  );
                },
              ),
            ),
          ),
          Container(
            width: 400,
            padding: ResponsiveWidget.isSmallScreen(context)
                ? const EdgeInsets.only(left: 45, right: 45, top: 30)
                : const EdgeInsets.only(top: 55),
            child: Text(
              MockData.menuData[currentIndex]["content"],
              style: GoogleFonts.lato(
                fontWeight: FontWeight.w500,
                fontSize: ResponsiveWidget.isSmallScreen(context) ? 21 : 40,
                color: const Color.fromRGBO(74, 85, 104, 1),
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ],
      ),
    );
  }

  scrollToCenter(int index, screenWidth) {
    if (index == 0) {
      _menuScrollController.animateTo(0, duration: const Duration(milliseconds: 500), curve: Curves.easeIn);
    } else {
      _menuScrollController.animateTo(index.toDouble() + screenWidth / 4,
          duration: const Duration(milliseconds: 500), curve: Curves.easeIn);
    }
  }

  Widget _buildMenuItem(bool isActive, String text, bool onHover) {
    return Container(
      width: 160,
      decoration: BoxDecoration(
        color: isActive
            ? AppColor.lightGreen
            : onHover
                ? AppColor.opaqueGreen
                : AppColor.whiteColor,
      ),
      child: Center(
          child: Text(
        text,
        style: GoogleFonts.lato(color: isActive ? AppColor.opaqueGreen : AppColor.darkGreen),
      )),
    );
  }
}
