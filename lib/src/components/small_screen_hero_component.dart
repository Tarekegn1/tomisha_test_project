import 'package:flutter/material.dart';
import 'package:test_project/src/common/app_color.dart';
import 'package:test_project/src/common/app_static_file.dart';
import 'package:test_project/src/common/app_text.dart';
import 'package:test_project/src/util/clippers/custom_first_wave_clipper.dart';

class SmallScreenHeroComponent extends StatelessWidget {
  const SmallScreenHeroComponent({super.key});

  @override
  Widget build(BuildContext context) {
    return ClipPath(
      clipper: CustomFirstWaveClipper(),
      child: Container(
        height: 660,
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              AppColor.opaqueGreenshGray,
              AppColor.opaqueGreen,
            ],
          ),
        ),
        child: Column(
          children: [
            const SizedBox(
              height: 48,
            ),
            Container(
              padding: const EdgeInsets.only(left: 40, right: 40, top: 18),
              alignment: Alignment.center,
              child: AppText.mediumText("Deine Job website", 42, AppColor.darkBlack),
            ),
            SizedBox(
              height: 410,
              child: Image.asset(
                AppStaticFile.agreementImage,
                alignment: Alignment.center,
                fit: BoxFit.cover,
              ),
            )
          ],
        ),
      ),
    );
  }
}
