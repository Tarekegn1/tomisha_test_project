import 'package:flutter/material.dart';
import 'package:test_project/src/common/app_color.dart';
import 'package:test_project/src/common/app_static_file.dart';
import 'package:test_project/src/common/app_text.dart';
import 'package:test_project/src/util/clippers/custom_second_wave_clipper.dart';

class SmallStepsComponent extends StatefulWidget {
  const SmallStepsComponent({super.key});

  @override
  State<SmallStepsComponent> createState() => _SmallStepsComponentState();
}

class _SmallStepsComponentState extends State<SmallStepsComponent> {
  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height;
    return SizedBox(
      height: screenHeight * 1.6,
      child: Stack(
        children: [
          ..._buildStepOneSection(),
          ..._buildStepThreeSection(),
          ..._buildStepTwoSection(),
        ],
      ),
    );
  }

  _buildStepOneSection() {
    return [
      Positioned(
        top: 70,
        left: -20,
        child: Container(
          height: 208,
          width: 208,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: AppColor.opaqueLightGray,
          ),
          child: Container(
            padding: const EdgeInsets.only(left: 40),
            child: AppText.regularText("1.", 130, AppColor.darkGrayish),
          ),
        ),
      ),
      Positioned(
        right: 40,
        top: 20,
        left: 100,
        child: Column(
          children: [
            SizedBox(
              height: 145,
              child: Image.asset(AppStaticFile.profileDataImage),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 50, top: 20, right: 40),
              child: AppText.regularText("Erstellen dein Unternehmensprofit", 16, AppColor.darkGrayish),
            )
          ],
        ),
      )
    ];
  }

  _buildStepTwoSection() {
    return [
      Positioned(
        top: 225,
        left: 0,
        right: 0,
        child: ClipPath(
          clipper: CustomSecondWaveClipper(),
          child: Container(
            height: 370,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  AppColor.opaqueGreen,
                  AppColor.opaqueGreenshGray,
                ],
                stops: [0.89, 1.0],
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
              ),
            ),
            child: Column(children: [
              Row(
                children: [
                  Container(
                    padding: const EdgeInsets.only(left: 60, top: 20),
                    child: AppText.regularText("2.", 130, AppColor.darkGrayish),
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 23, top: 90),
                    child: Expanded(
                      child: AppText.regularText(
                        "Erhalte Vermittlungs- \nangebot von Arbeitgeber",
                        16,
                        AppColor.darkGrayish,
                      ),
                    ),
                  )
                ],
              ),
              Container(
                padding: const EdgeInsets.only(left: 40),
                width: 218,
                child: Image.asset(AppStaticFile.jobOffset, fit: BoxFit.cover),
              )
            ]),
          ),
        ),
      )
    ];
  }

  _buildStepThreeSection() {
    return [
      Positioned(
        top: 540,
        left: -50,
        child: Container(
          height: 300,
          width: 300,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Color.fromRGBO(247, 250, 252, 1),
          ),
        ),
      ),
      Positioned(
        top: 540,
        left: 51,
        right: 51,
        child: Container(
          alignment: Alignment.centerLeft,
          child: Column(
            children: [
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: AppText.regularText("3.", 130, AppColor.darkGrayish),
                  ),
                  const SizedBox(
                    width: 24,
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 90.0),
                      child: AppText.regularText(
                        "Vermittlung nach Provision oder Stundenlohn",
                        16,
                        AppColor.darkGrayish,
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                padding: const EdgeInsets.only(left: 40),
                width: 250,
                child: Image.asset(
                  AppStaticFile.businessCard,
                  fit: BoxFit.cover,
                ),
              ),
            ],
          ),
        ),
      )
    ];
  }
}
