import 'package:flutter/material.dart';
import 'package:test_project/src/common/app_color.dart';
import 'package:test_project/src/common/app_static_file.dart';
import 'package:test_project/src/common/app_text.dart';
import 'package:test_project/src/components/custom_btn.dart';
import 'package:test_project/src/util/clippers/custom_first_wave_clipper.dart';

class LargeScreenHeroComponent extends StatelessWidget {
  final VoidCallback onClicked;
  const LargeScreenHeroComponent({
    super.key,
    required this.onClicked,
  });

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return ClipPath(
      clipper: CustomFirstWaveClipper(),
      child: Container(
        height: 660,
        width: double.infinity,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              AppColor.opaqueGreenshGray,
              AppColor.opaqueGreen,
            ],
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: 320,
                  alignment: Alignment.center,
                  child: AppText.boldText(
                    "Deine Job website",
                    AppColor.darkBlack,
                  ),
                ),
                const SizedBox(height: 53),
                CustomButton(onClicked: onClicked),
              ],
            ),
            SizedBox(
              width: 0.076 * screenWidth,
            ),
            Column(
              children: [
                const SizedBox(height: 80),
                Container(
                  height: 455,
                  width: 455,
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.white,
                    image: DecorationImage(
                      image: AssetImage(AppStaticFile.agreementImage),
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
