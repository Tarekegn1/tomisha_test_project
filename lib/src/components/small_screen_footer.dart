import 'package:flutter/material.dart';
import 'package:test_project/src/components/custom_btn.dart';

class SmallScreenFooter extends StatelessWidget {
  const SmallScreenFooter({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 128,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(12),
          topRight: Radius.circular(12),
        ),
        boxShadow: [
          BoxShadow(
            blurRadius: 4,
            spreadRadius: 1,
            color: Color.fromRGBO(0, 0, 0, 0.2),
          ),
        ],
      ),
      child: Center(
        child: CustomButton(
          onClicked: () {},
        ),
      ),
    );
  }
}
