import 'package:flutter/material.dart';
import 'package:test_project/src/common/app_color.dart';
import 'package:test_project/src/common/app_text.dart';

class NavBarComponent extends StatefulWidget {
  const NavBarComponent({super.key});

  @override
  State<NavBarComponent> createState() => _NavBarComponentState();
}

class _NavBarComponentState extends State<NavBarComponent> {
  bool isHovered = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 67,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomLeft: Radius.circular(12),
          bottomRight: Radius.circular(12),
        ),
        boxShadow: [
          BoxShadow(
            blurRadius: 6,
            spreadRadius: 3,
            color: AppColor.topShadowColor,
          )
        ],
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            height: 10,
            decoration: const BoxDecoration(
                gradient: LinearGradient(
              colors: [
                AppColor.darkGreen,
                AppColor.darkBlue,
              ],
            )),
          ),
          Container(
            padding: const EdgeInsets.only(right: 17, top: 26),
            child: Center(
              child: Container(
                alignment: Alignment.centerRight,
                child: MouseRegion(
                  onExit: (val) {
                    setState(() {
                      isHovered = false;
                    });
                  },
                  onHover: (val) {
                    setState(() {
                      isHovered = true;
                    });
                  },
                  child: AppText.semiBoldText("Login", 14, AppColor.darkGreen),
                ),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(right: 15),
            width: 35,
            height: 1.5,
            color: isHovered ? AppColor.darkGreen : AppColor.whiteColor,
          )
        ],
      ),
    );
  }
}
