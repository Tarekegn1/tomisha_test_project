import 'package:flutter/material.dart';
import 'package:test_project/src/common/app_color.dart';
import 'package:test_project/src/common/app_static_file.dart';
import 'package:test_project/src/common/app_text.dart';
import 'package:test_project/src/util/clippers/custom_second_wave_clipper.dart';

class LargeStepComponent extends StatelessWidget {
  const LargeStepComponent({super.key});

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return Stack(
      children: [
        Column(
          children: [
            _buildStepOneSection(screenWidth),
            const SizedBox(
              height: 200,
            ),
            _buildStepTwoSection(screenWidth),
            const SizedBox(height: 150),
            _buildStepThreeSection(screenWidth),
            const SizedBox(height: 200),
          ],
        ),
        Positioned(
          top: 0.082 * screenHeight,
          left: 0.27 * screenWidth,
          child: Container(
            width: 0.25 * screenWidth,
            padding: const EdgeInsets.only(top: 100.0, left: 10),
            child: Expanded(
              child: AppText.semiBoldText("Erstellen dein Lebenslauf", 30, AppColor.darkGrayish),
            ),
          ),
        ),
        _buildOverLayLineOne(screenHeight, screenWidth),
        _buildOverLayLineTwo(screenHeight, screenWidth),
        Positioned(
          top: 1070,
          left: 0.41 * screenWidth,
          child: Container(
            alignment: Alignment.centerLeft,
            width: 275,
            child: Expanded(
              child: AppText.semiBoldText(
                "Mit nur einem Klick bewerben",
                30,
                AppColor.darkGrayish,
              ),
            ),
          ),
        )
      ],
    );
  }

  _buildOverLayLineOne(double screenHeight, double screenWidth) {
    return Positioned(
      top: 0.32 * screenHeight,
      left: 0.25 * screenWidth,
      child: SizedBox(
        width: 0.41 * screenWidth,
        child: Image.asset(AppStaticFile.stepOneImage),
      ),
    );
  }

  _buildOverLayLineTwo(double screenHeight, double screenWidth) {
    return Positioned(
      top: 790,
      right: 0.28 * screenWidth,
      child: SizedBox(
        width: 0.35 * screenWidth,
        child: Image.asset(AppStaticFile.stepTwoImage),
      ),
    );
  }

  _buildStepOneSection(double screenWidth) {
    return Row(
      children: [
        SizedBox(width: 0.15 * screenWidth),
        Container(
          height: 208,
          width: 208,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Color.fromRGBO(247, 250, 252, 1),
          ),
          child: Center(
            child: AppText.regularText("1.", 130, AppColor.darkGrayish),
          ),
        ),
        SizedBox(
          width: 0.23 * screenWidth,
        ),
        SizedBox(
          height: 0.2 * screenWidth,
          child: Image.asset(AppStaticFile.profileDataImage),
        )
      ],
    );
  }

  _buildStepTwoSection(double screenWidth) {
    return ClipPath(
      clipper: CustomSecondWaveClipper(),
      child: Container(
        height: 370,
        decoration: const BoxDecoration(
          gradient: LinearGradient(
            colors: [
              AppColor.opaqueGreen,
              AppColor.opaqueGreenshGray,
            ],
            stops: [0.89, 1.0],
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
          ),
        ),
        child: Row(
          children: [
            SizedBox(width: 0.35 * screenWidth),
            SizedBox(
              width: 0.22 * screenWidth,
              child: Image.asset(AppStaticFile.undrawImage),
            ),
            SizedBox(width: 0.06 * screenWidth),
            Padding(
              padding: const EdgeInsets.only(top: 30.0, right: 23),
              child: AppText.regularText("2.", 130, AppColor.darkGrayish),
            ),
            Expanded(
              child: Padding(
                  padding: const EdgeInsets.only(top: 100.0),
                  child: AppText.semiBoldText("Erstellen dein Lebenslauf", 30, AppColor.darkGrayish)),
            )
          ],
        ),
      ),
    );
  }

  _buildStepThreeSection(double screenWidth) {
    return Row(
      children: [
        SizedBox(
          width: 0.25 * screenWidth,
        ),
        Container(
          height: 300,
          width: 300,
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: Color.fromRGBO(247, 250, 252, 1),
          ),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: AppText.regularText("3.", 130, AppColor.darkGrayish),
              ),
            ],
          ),
        ),
        SizedBox(
          width: 0.12 * screenWidth,
        ),
        SizedBox(
          width: 0.25 * screenWidth,
          child: Image.asset(AppStaticFile.personalImage, fit: BoxFit.cover),
        )
      ],
    );
  }
}
