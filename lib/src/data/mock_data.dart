class MockData {
  static List<Map<String, dynamic>> menuData = [
    {"name": "Arbeitnehmer", "content": "Drei einfache Schritte zu deinem neuen Job"},
    {"name": "Arbeitgeber", "content": "Drei einfache Schritte zu deinem neuen Mitarbeiter"},
    {"name": "Temporärbüro", "content": "Drei einfache Schritte zur Vermittlung neuer Mitarbeiter"}
  ];
}
